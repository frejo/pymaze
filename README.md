# PyMaze

![A BIG generated Maze](maze.png)

A very naive maze generator written in Python for Gazebo.

## Install

Assuming your catkin workspace is located at `~/catkin_ws`

```bash
cd ~/catkin_ws/src
git clone https://gitlab.com/frejo/pymaze.git
cd ..
catkin_make
```

## Run

The script can be run using ROS or directly. Both methods will save the 
generated mazes in `~/catkin_ws/src/pymaze/{models,worlds,launch}`.


### Using ROS

To simply generate a standard maze (5x5), run

```bash
rosrun pymaze sdf_gen.py
```

run 
```bash
rosrun pymaze sdf_gen.py -h
```
to see the available options.

### Using python
Run
```bash
cd ~/catkin_ws/src/pymaze/scripts/pymaze
python3 sdf_gen.py -h
```
to see the available options.

# License

The maze generation script is from [rosettacode.org](http://rosettacode.org/wiki/Maze_generation#Python) and is licensed under the [GNU Free Documentation License 1.2](https://www.gnu.org/licenses/old-licenses/fdl-1.2.html). A copy of this license can be found in [GFDL.md](GFDL.md)

All other parts of the software are licensed under the [MIT license](LICENSE.md) unless otherwise noted.
