#%%
import lxml.etree as etree
import os

def create_sdf(model_name, links, static:bool = False):
    # Create sdf model element with the given links
    sdf = etree.Element('sdf')
    sdf.set('version', '1.7')
    model = etree.SubElement(sdf, 'model')
    model.set('name', model_name)
    
    for link in links:
        model.append(link)

    etree.SubElement(model, 'static').text = str(int(static))
    etree.SubElement(model, 'allow_auto_disable').text = '0'
    return sdf

def create_link(link_name, pos, visual, collision, mass='1', self_collide=False):
    # Create a link element with the given data
    link = etree.Element('link')
    link.set('name', link_name)

    inertial = etree.SubElement(link, 'inertial')
    etree.SubElement(inertial, 'mass').text = mass

    inertia = etree.SubElement(inertial, 'inertia')
    for s in ['ixx', 'ixy', 'ixz', 'iyy', 'iyz', 'izz']:
        etree.SubElement(inertia, s).text = '0'
    
    etree.SubElement(inertial, 'pose').text = '0 0 0 0 -0 0'

    etree.SubElement(link, 'pose').text = pos
    etree.SubElement(link, 'self_collide').text = str(int(self_collide))
    etree.SubElement(link, 'enable_wind').text = '0'
    etree.SubElement(link, 'kinematic').text = '0'
    
    link.append(visual)
    link.append(collision)

    return link

def create_visual(geometry, path, pose=None, material_name=None, transparency:int = 0, cast_shadows:bool = True):
    # Create a visual with given geometry based on the template in visual.xml

    tree = etree.parse(os.path.join(path, 'templates/visual.xml'))
    
    # Add geometry data
    tree.getroot().append(geometry)

    # Change default pose data
    if pose:
        tree.xpath('/visual/pose')[0].text = pose

    # Change default material name
    if pose:
        tree.xpath('/visual/material/name')[0].text = material_name

    # Change default transparency
    if pose:
        tree.xpath('/visual/transparency')[0].text = str(transparency)

    # Change default transparency
    if pose:
        tree.xpath('/visual/cast_shadows')[0].text = str(int(cast_shadows))

    return tree.getroot()

def create_polyline_geometry(height, lines):
    # Create a geometry of polylines from the given list of lines
    geo = etree.Element('geometry')
    for line in lines:
        poly = etree.SubElement(geo, 'polyline')
        etree.SubElement(poly, 'height').text = str(height)
        for point in line:
            etree.SubElement(poly, 'point').text = f'{point[0]} {point[1]}'
    return geo

def create_collision(geometry, path, pose=None):
    # Create a collision with given geometry based on the template in collision.xml

    tree = etree.parse(os.path.join(path, 'templates/collision.xml'))
    tree.getroot().append(geometry)

    # Change default pose data
    if pose:
        tree.xpath('/collision/pose')[0].text = pose
    
    return tree.getroot()

def pprint(el):
    # shorthand for pretty printing an xml element
    print(etree.tostring(el, pretty_print=True, encoding = "unicode"))

def save_sdf(el, filename):
    # Save an xml element to a file. Creates the path to the file if it doesn't exist
    et = etree.ElementTree(el)
    if not os.path.exists(os.path.dirname(filename)):
        os.makedirs(os.path.dirname(filename))
    et.write(filename, xml_declaration=True, encoding='UTF-8', pretty_print=True)

def config_gen(name, 
               desciption='Randomly generated maze', 
               author_name='', 
               author_email='', 
               version='1.0',
               sdf_path='model.sdf', 
               sdf_version='1.7'):
    # Make a basic model config file

    mod = etree.Element('model')
    etree.SubElement(mod, 'name').text = name
    etree.SubElement(mod, 'version').text = version
    sdf = etree.SubElement(mod, 'sdf')
    sdf.set('version', sdf_version)
    sdf.text = sdf_path
    auth = etree.SubElement(mod, 'author')
    etree.SubElement(auth, 'name').text = author_name
    etree.SubElement(auth, 'email').text = author_email
    etree.SubElement(mod, 'desciption').text = desciption
    return mod

def save_model(el, name, path='models/'):
    # Save a model by saving the model sdf and config
    if not os.path.exists(path+name):
        os.makedirs(path+name)
    save_sdf(el, path+name+'/model.sdf')
    save_sdf(config_gen(name), path+name+'/model.config')

def gen_world(name, models, path=''):
    # Generate basic world xml element with the given models
    tree = etree.parse(os.path.join(path, 'templates/default.world'))
    sdf = tree.getroot()
    world = sdf[0]
    world.set('name', name)
    for model in models:
        world.append(model)
    return sdf

def gen_launch(world_name, tb_start:dict=None, path=''):
    # Generate a launch xml element to launch a world with the given name. tb_start can be set to change the default position of the turtlebot
    tree = etree.parse(os.path.join(path, 'templates/default.launch'))
    for _ in tree.xpath('/launch/include/arg[@name="world_name"]'):
        _.set('value', f"$(find pymaze)/worlds/{world_name}.world") 
    
    for key in tb_start:
        tree.xpath(f'/launch/arg[@name="{key}"]')[0].set('default', str(tb_start[key]))

    return tree.getroot()

# %%
if __name__ == "__main__":
    path =  os.path.dirname(
                os.path.dirname(
                    os.path.dirname(
                        os.path.realpath(__file__)
                    )
                )
            )

# %%
