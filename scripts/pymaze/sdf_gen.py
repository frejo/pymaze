'''
Create a maze in Gazebo-readable sdf to test a Turtlebot in.
'''

#%%
from datetime import datetime
from random import randrange
import os, sys
import argparse

PATH = ''
try:
    #  When running the script directly, the path in which to save files is two directories above the script file
    from maze import make_maze
    import sdf_content as SDF
    PATH =  os.path.dirname(
                os.path.dirname(
                    os.path.dirname(
                        os.path.realpath(__file__)
                    )
                )
            )
except:
    # When running with ROS, the root path is the package directory
    from pymaze.maze import make_maze
    import pymaze.sdf_content as SDF
    import rospkg
    PATH = rospkg.RosPack().get_path('pymaze')

PATH = os.path.join(PATH, '')
print(f'Using {PATH} as root directory. Files will be saved under here.')

# Global variables for widths
WALL_WIDTH = 0.25
PATH_WIDTH = 0.50

#%%
def maze_str_idx(x, y, cells_x):
    # Get index of a cell position in a maze made by maze.py that is cells_x wide
    return y*(cells_x*2+2) + x


def horizontal_walls(s, width:int, height:int):
    '''
    Returns a list of coordinates for all horizontal walls ('-') in the given maze string s

    s: maze string
    width: number of horizontal cells
    height: number of vertical cells
    '''
    wall_list = []
    print(width, height)
    for r in range(height+1):
        for c in range(width):
            if s[maze_str_idx(2*c+1, 2*r, width)] == '-':
                wall_list.append((2*c+1, 2*r))
    return wall_list

def vertical_walls(s, width:int, height:int):
    '''
    Returns a list of coordinates for all vertical walls ('|') in the given maze string s

    s: maze string
    width: number of horizontal cells
    height: number of vertical cells
    '''
    wall_list = []
    for r in range(height):
        for c in range(width+1):
            if s[maze_str_idx(2*c, 2*r+1, width)] == '|':
                wall_list.append((2*c, 2*r+1))
    return wall_list

def wall_points(pos):
    # Returns a list points 


    '''
    Example maze:
        +-+-+-+
        |     |
        +-+-+ +
        |   | |
        + + + +
        | |   |
        +-+-+-+
    
    vertical walls have even x-coordinates
    horizontal walls have odd x-coordinates
    '''
    if pos[0]%2 == 0:
        # vertical
        x = (WALL_WIDTH+PATH_WIDTH)*(pos[0])/2
        y = (WALL_WIDTH+PATH_WIDTH)*((pos[1]-1)/2)

        points = [
            (x           , y),
            (x+WALL_WIDTH, y),
            (x+WALL_WIDTH, y+2*WALL_WIDTH+PATH_WIDTH),
            (x           , y+2*WALL_WIDTH+PATH_WIDTH),
            (x           , y)
        ]
    else:
        # horizontal
        x = (WALL_WIDTH+PATH_WIDTH)*(pos[0]-1)/2
        y = (WALL_WIDTH+PATH_WIDTH)*(pos[1])/2

        points = [
            (x                        , y),
            (x+2*WALL_WIDTH+PATH_WIDTH, y),
            (x+2*WALL_WIDTH+PATH_WIDTH, y+WALL_WIDTH),
            (x                        , y+WALL_WIDTH),
            (x                        , y)
        ]
    return points

# %%
def maze_to_polyline_geometry(str_maze, height, cells_x:int, cells_y:int):
    # Generate a single geometry with multiple polylines; one for each wall part.
    #   DOESN'T WORK! Gazebo crashes because of overlapping geometry.
    lines = []
    for wall in horizontal_walls(str_maze, cells_x, cells_y):
        #print(wall_points(wall))
        lines.append(wall_points(wall))
    #print("")
    for wall in vertical_walls(str_maze, cells_x, cells_y):
        #print(wall_points(wall))
        lines.append(wall_points(wall))
    
    geo = SDF.create_polyline_geometry(height, lines)
    return geo

def links_from_walls(str_maze, height, cells_x:int, cells_y:int, material='Gazebo/Blue', remove_first_wall=False):
    '''
    Make a list of links, each one containing a single wall part

    - str_maze: maze string from maze.py
    - height:   height in meters for the wall parts (float)
    - cells_x:  number of cells in the x direction
    - cells_y:  number of cells in the y direction
    - material: gazebo material for the walls. Currently has to be from the default gazebo.material script
    - remove_first_wall: Remove the first wall in the maze (south of cell (0,0))
    '''
    links = []

    lines = []
    for wall in horizontal_walls(str_maze, cells_x, cells_y):
        lines.append(wall_points(wall))
    for wall in vertical_walls(str_maze, cells_x, cells_y):
        lines.append(wall_points(wall))

    idx = 0
    for line in lines:
        link = SDF.create_link(
            f'wall_{idx}', # link name
            '0 0 0 0 0 0', # link pose
            SDF.create_visual(SDF.create_polyline_geometry(height, [line]), PATH, material_name=material),
            SDF.create_collision(SDF.create_polyline_geometry(height, [line]), PATH),
            mass='20000'
        )
        links.append(link)
        idx+=1
    if remove_first_wall:
        links.pop(0)
    return links

def generator(cells_x:int, cells_y:int, maze_name=None, tb_args=None, remove_first_wall=False):
    '''
    Generates a maze model, world, and launchfile of the given size.

    - cells_x: size in x direction
    - cells_y: size in y direction
    - (optional) maze_name: name of the maze. If none given, it will be based on the currecnt time stamp
    - (optional) tb_args: dictionary of arguments to change the default value of in the launch file
    - (optional) remove_first_wall: Removes the first wall (south of cell (0,0))
    '''
    maze = make_maze(cells_x, cells_y, (0,0))

    if not maze_name:
        # Generate a name if none is given
        now = datetime.now()
        dt_string = now.strftime("%Y-%m-%dT%H%M%S")
        maze_name = f'random_maze_{dt_string}'
    
    # Calculate the center of the maze, so it can be positioned with the south west corner in (0,0)
    center_x = (PATH_WIDTH*cells_x + WALL_WIDTH*(cells_x+1))/2
    center_y = (PATH_WIDTH*cells_y + WALL_WIDTH*(cells_y+1))/2
    height   = 0.5

    # Create the model sdf
    sdf = SDF.create_sdf(maze_name, links_from_walls(maze, height, cells_x, cells_y, remove_first_wall=remove_first_wall), static=True)

    SDF.save_model(sdf, maze_name, path=os.path.join(PATH, 'models/'))
    print(f"Generated maze {maze_name}")

    # Generate and save the world
    SDF.save_sdf(
        SDF.gen_world(maze_name, sdf, PATH),
        os.path.join(PATH, 'worlds/', f'{maze_name}.world')
    )

    # Generate and save the launch file
    SDF.save_sdf(
        SDF.gen_launch(maze_name, path=PATH, tb_start=tb_args),
        os.path.join(PATH, 'launch/', f'{maze_name}.launch')
    )

    return maze_name

# %%
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generate a random maze for use in Gazebo with a Turtlebot 3')
    parser.add_argument('-n', '--name', type=str, default=None, action='store', 
                        help='Set a name. Otherwise it will be called "random_maze_yyy-mm-ddThhmmss"')
    parser.add_argument('-x', help='Number of cells in the x-direction', type=int, default=5, action='store')
    parser.add_argument('-y', help='Number of cells in the y-direction', type=int, default=5, action='store')
    parser.add_argument('-r', '--random_start', help='Start the turtlebot at a random location', action='store_true')
    parser.add_argument('-f', '--remove_first', help='Remove the first wall (south of cell (0,0))', action='store_true')
    parser.add_argument('-l', '--launch', help='Launch Gazebo after generating the maze. Maze is deleted afterwards unless --keep is set', action='store_true')
    parser.add_argument('-k', '--keep', help='Select to keep maze files after launching. Does nothing if -l/--launch isn\'t set', action='store_true')
    parser.add_argument('--wall_width', help="Set the width of walls", default=0.25, type=float, action='store')
    parser.add_argument('--path_width', help="Set the width of paths", default=0.50, type=float, action='store')
    args = parser.parse_args()
    #print(args)

    # Set global setting variables
    WALL_WIDTH = args.wall_width
    PATH_WIDTH = args.path_width

    # Set changed default values for the turtlebot
    launch_args = {}
    if args.random_start:
        launch_args['x_pos'] = randrange(args.x)*(WALL_WIDTH+PATH_WIDTH) + PATH_WIDTH
        launch_args['y_pos'] = randrange(args.y)*(WALL_WIDTH+PATH_WIDTH) + PATH_WIDTH

    # Generate the maze files and store the name
    maze_name = generator(
        args.x, 
        args.y, 
        args.name, 
        tb_args=launch_args, 
        remove_first_wall=args.remove_first
    )

    if args.launch:
        os.system(f'roslaunch pymaze {maze_name}.launch')
        if not args.keep:
            # Remove the files after Gazebo has been closed down
            os.system(f'rm -r {os.path.join(PATH, "models/", maze_name)}')
            os.remove(os.path.join(PATH, f'worlds/{maze_name}.world'))
            os.remove(os.path.join(PATH, f'launch/{maze_name}.launch'))

    pass

# %%
